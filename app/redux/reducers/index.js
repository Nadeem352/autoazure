import { combineReducers } from 'redux';
import * as loginReducer from './login';

export default rootReducer =  combineReducers(Object.assign(
    loginReducer,
))
