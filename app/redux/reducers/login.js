
import * as types from '../actions/types';
const initialState = {
    data: null,
    loading: false,
    error: null,
    status: null,
}
export const loginData = (state = initialState, action) => {
    switch (action.type) {
        case types.RESET_STATE:
            return initialState;
            break;
        case types.LOGIN_CALL_REQUEST:
            return {
                ...state,
                loading: true
            }
            break;
        case types.LOGIN_CALL_SUCCESS:
            if(action.payload != null){
                return {
                    ...state,
                    data: action.payload,
                    status: 200,
                    loading: false,
                }
            }
            return {
                ...state,
                data: action.payload,
                status: 401,
                loading: false,
                error: 'Invalid credentials'
            }
        case types.LOGIN_CALL_FAILURE:
            return {
                ...state,
                data: null,
                status: 500,
                loading: false,
                error: 'Network Request failed'
            }
        default:    
        return state
            
    }
}