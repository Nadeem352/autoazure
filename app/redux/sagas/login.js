import { takeLatest, call, put } from "redux-saga/effects";
import * as types from '../actions/types';
import Api from '../../lib/api';

export function* loginRequest() {
  yield takeLatest(types.LOGIN_CALL_REQUEST, login);
}
function* login(params) {
  try {
    const response = yield Api.post('auth/login', params.params);
    console.log('response is', response);
    
    // dispatch a success action to the store with the new dog

    yield put({ type: types.LOGIN_CALL_SUCCESS, payload: response.d });
  } catch (error) {    
      yield put({ type: types.LOGIN_CALL_FAILURE, error: 'Network Error' });    
  }
}