import * as types from './types';

  export const login = (params) => {
    return  {
       type: types.LOGIN_CALL_REQUEST ,
       params
      }
  }
  export const resetSate = () => {
    return  {
       type: types.RESET_STATE
      }
  }