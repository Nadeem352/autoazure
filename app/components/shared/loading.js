import React, { Component } from 'react';

import { ActivityIndicator, View } from 'react-native'

export default Loading= (props) => {
  return (
    <View style = { props.styles }>
      <ActivityIndicator size="large" />
    </View>
  )
}
