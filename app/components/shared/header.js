import React, { Component } from 'react';
import { TouchableOpacity, View, Text, ImageBackground, Image } from 'react-native';
import { headerStyles } from '../styles/headerStyles';

 const Header = (props) => {
    return (
        <View style={headerStyles.container}>
            <TouchableOpacity style={headerStyles.leftButton} onPress = { props.onLeftPress }>
            </TouchableOpacity>
            <View style={headerStyles.title}>
                <Text style={{ color: props.textColor, fontSize: 20, fontWeight: 'bold' }}>{ props.title }</Text>
            </View>
            <TouchableOpacity style={headerStyles.rightButton} onPress = { props.onRightPress }  >
            </TouchableOpacity>
        </View>
    )
}
const HeaderLeft = (props) => {
    return (
        <View style={headerStyles.container}>
            <TouchableOpacity style={headerStyles.leftButton} onPress = { props.onPress }>
            </TouchableOpacity>
            <View style={headerStyles.title}>
                <Text style={{ color: props.titleColor, fontSize: 20, fontWeight: 'bold' }}>{ props.title }</Text>
            </View>
            <View style={headerStyles.rightButton}  >
            </View>
        </View>
    )
}
export { Header, HeaderLeft }