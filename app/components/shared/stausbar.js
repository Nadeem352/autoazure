import React from 'react';
import { StatusBar, Platform } from 'react-native';

export const StatusTopBar = () => (
    <StatusBar
        backgroundColor="white"
        barStyle={
            Platform.OS === "ios"
                ? "light-content"
                : "dark-content"}
    />
)
