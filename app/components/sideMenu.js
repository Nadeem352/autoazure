import React, { Component } from 'react';
import { ScrollView, TouchableOpacity, View, Text, ImageBackground, Image } from 'react-native';
import PropTypes from 'prop-types';
import { drawerStyles } from './styles/drawerStyles';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { login } from '../redux/actions/login';
import { Alert } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation';


class SideMenu extends Component {
    navigateToScreen = (route, type) => async () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        await this.props.navigation.dispatch(navigateAction);
        if (type === 'Logout') {
            this.logout()
        }
    }
    logout = () => {
        Alert.alert(
            'Logout',
            'Do you really want to logout?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Yes', onPress: async () => {
                        await this.props.navigation.closeDrawer();
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'Login' })],
                          });
                          this.props.navigation.dispatch(resetAction);
                    }
                },
            ],
            { cancelable: false },
        );
    }
    render() {
        const { loginData } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <View style={drawerStyles.profileView}>
                    <Text style={drawerStyles.welComeText} >Welcome to Real-Time Messaging App</Text>
                </View>
                <View style={drawerStyles.mainView}>
                   

                    <TouchableOpacity style={drawerStyles.button} onPress={this.navigateToScreen('Home')}>
                        <Text >Home</Text>
                    </TouchableOpacity>
                    {
                        loginData &&
                        <TouchableOpacity style={drawerStyles.button} onPress={this.navigateToScreen('Home', 'Logout')}>
                            <Text >Logout</Text>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    let data = state.loginData;
    return {
        loading: data.loading,
        status: data.status,
        error: data.error,
        loginData: data.data
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu)