
import React, { Component } from 'react';
import { Alert } from 'react-native';

export default AlertBox = (props) => {    
    Alert.alert(
        props.title,
        props.error,
        [
          {
            text: 'Ok',
            onPress: () => console.log('Cancel Pressed')
          },
        ],
       
        );
}