import React, { Component } from 'react';
import {
  Text,View,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import PropTypes from '../../../node_modules/prop-types';
import { Colors,Constants } from '@common';
const { fontSize, Window } = Constants;
const { width } = Window;

export default class LoadingButton extends Component<{}> {
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    loadingColor: PropTypes.string,
    buttonStyle: PropTypes.oneOfType(
      [
        PropTypes.object,
        PropTypes.array
      ]),
    leftSource: PropTypes.object,
    rightSource: PropTypes.object,
    leftImageStyle: PropTypes.oneOfType(
      [
        PropTypes.object,
        PropTypes.array
      ]),
    rightImageStyle: PropTypes.oneOfType(
      [
        PropTypes.object,
        PropTypes.array
      ]),
    titleStyle: PropTypes.oneOfType(
    [
      PropTypes.object,
      PropTypes.array
    ]),
    tooltipText: PropTypes.string,

  };

  state = {
      isLoading: false,
  };

  showLoading(isLoading) {
    if (isLoading) {
      this.setState({ isLoading: isLoading });
    } else {
      this.setState({ isLoading: isLoading });
    }
  }

  render() {
    return(
      <TouchableOpacity activeOpacity={1} onPress={!this.props.isLoading ? this.props.onPress : null} style={[styles.button, this.props.buttonStyle]}>
        {
          this.props.isLoading
          ?
          <ActivityIndicator size="small" color={this.props.loadingColor || Colors.white} />
          :
          <View style={[styles.buttonView,this.props.textViewAlign]}>
            <Text style={[styles.title,this.props.titleStyle]} >{this.props.title}</Text>
          </View>
        }
      </TouchableOpacity>
    );
  }
}

const styles = {
  button: {
    height: 50,
    width: width - 40,
    backgroundColor: Colors.blue,
    borderRadius: 5,
    justifyContent: 'center',
    marginVertical:20,
  },
  title: {
    letterSpacing: 2,
    color: Colors.white,
    fontSize: fontSize.large,
  },
  buttonView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
};
