import React, { Component } from 'react';
import {  View, Text, Image, TouchableOpacity } from 'react-native';
import { Constants, Colors } from '../../common/index';
const { fontSize, Window } = Constants;

export const Button = (props) => (
    <TouchableOpacity style = {[{
        height: 48,
        width: Window.width - 40,
        backgroundColor: props.backgroundColor,
        borderColor: props.borderColor,
        borderRadius:5,
        alignItems:'center',
        justifyContent:'center',
        marginTop:80,

        },props.style]}
        onPress = { props.onPress }
        >
        <Text style = {{ color: props.color, }}>{ props.text }</Text>
    </TouchableOpacity>
)
