import { StyleSheet } from 'react-native'

export const headerStyles = StyleSheet.create({
    container:{   
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'transparent',
        flexDirection:'row'
    },
    leftButton:{
        flex:0.2,
        height:30,
        alignItems:'center',
        justifyContent:'center',
    },
    title:{
        flex:0.6,
        alignItems:'center',
        justifyContent:'center',
    },
    rightButton:{
        flex:0.2,
        alignItems:'center',
        justifyContent:'center',
    }
})

