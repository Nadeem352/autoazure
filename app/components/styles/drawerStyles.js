import { StyleSheet } from 'react-native'

export const drawerStyles = StyleSheet.create({
    profileView:{   
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#FD4E62'
    },
    mainView:{
        flex:3.5, 
        alignItems:'center', 
        justifyContent:'flex-start',
        backgroundColor:'lightgrey'
    },
    button:{
        alignItems:'center',
        justifyContent:'center',
        height:50,
        width: 280,
        borderBottomWidth:0.5,
        borderBottomColor:'grey'
    },
    welComeText:{
        fontSize:15,
        fontWeight:'bold',
        color:'white'
    }
   
})

