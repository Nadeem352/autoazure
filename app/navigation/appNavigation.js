import React from 'react';
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import {
  createAppContainer,
  createSwitchNavigator,
  NavigationActions,
} from "react-navigation";
import { Platform, View } from 'react-native';

import Login from '../containers/authFlow/login';
import Indicadores from '../containers/appFlow/Indicadores';
import Mensajes from '../containers/appFlow/Mensajes/Mensajes';
import ChatScreen from '../containers/appFlow/Mensajes/ChatScreen';
import Drawr from '../containers/appFlow/Drawr/Drawr';
import More from '../containers/appFlow/More/More';
import Preguntas from '../containers/appFlow/Preguntas';
import Ventas from '../containers/appFlow/Ventas';
import DetaliScreen from '../containers/appFlow/Preguntas/DetailScreen';
import RespuestasRapidas from '../containers/appFlow/Preguntas/RespuestasRapidas';
import HistorialDePreguentes from '../containers/appFlow/Drawr/HistorialDePreguentes';

import Ionicons from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionic from 'react-native-vector-icons/Ionicons';
import { Colors } from '@common';
import { createDrawerNavigator, DrawerActions } from 'react-navigation-drawer';

const IndicadoresStack = createStackNavigator({
  Indicadores: {
    screen: Indicadores,
    navigationOptions: {
      title: 'Indicadores',
      headerStyle: {
        backgroundColor: Colors.blue,
      },
      headerTitleStyle: {
        color: Colors.white,
        textAlign: 'center', alignSelf: 'center'
      }
    }
  }
}, {
  headerLayoutPreset: 'center',
  initialRouteName: "Indicadores",
});

const MensajesStack = createStackNavigator({
  Mensajes: {
    screen: Mensajes,
    navigationOptions: {
      title: 'Mensajes',
      headerStyle: {
        backgroundColor: Colors.blue
      },
      headerTitleStyle: {
        color: Colors.white
      },
    },
  },
  ChatScreen: {
    screen: ChatScreen,
    navigationOptions: {
      title: 'ChatScreen',
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: Colors.blue,
        headerTintColor: 'white',
      },
      headerTitleStyle: {
        color: Colors.white,
      },
    },
  },
}, {
  headerLayoutPreset: 'center',
  initialRouteName: "Mensajes",
});

const PreguntasStack = createStackNavigator({
  Preguntas: {
    screen: Preguntas,
    navigationOptions: {
      title: 'Preguntas',
      headerStyle: {
        backgroundColor: Colors.blue
      },
      headerTitleStyle: {
        color: Colors.white
      },
    }
  },
  DetaliScreen: {
    screen: DetaliScreen,
    navigationOptions: {
      title: 'Preguntas Detail',
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: Colors.blue
      },
      headerTitleStyle: {
        color: Colors.white
      },
    }
  },
  HistorialDePreguentes: {
    screen: HistorialDePreguentes,
    navigationOptions: {
      title: 'HistorialDePreguentes',
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: Colors.blue
      },
      headerTitleStyle: {
        color: Colors.white
      },
    }
  },
  RespuestasRapidas: {
    screen: RespuestasRapidas,
    navigationOptions: {
      title: 'Respuestas Rapidas',
      headerStyle: {
        backgroundColor: Colors.blue
      },
      headerTitleStyle: {
        color: Colors.white
      },
      headerTintColor: '#fff'
    }
  },
}, {
  headerLayoutPreset: 'center',
  initialRouteName: "Preguntas",
});

const VentasStack = createStackNavigator({
  Ventas: {
    screen: Ventas,
    navigationOptions: {
      title: 'Ventas',
      headerStyle: {
        backgroundColor: Colors.blue,
      },
      headerTitleStyle: {
        color: Colors.white
      },
    }
  }
}, {
  headerLayoutPreset: 'center',
  initialRouteName: "Ventas",
});

const MoreStack = createStackNavigator({
  More: {
    screen: More,
    navigationOptions: {
      title: 'More',
      headerStyle: {
        backgroundColor: Colors.blue
      },
      headerTitleStyle: {
        color: Colors.white
      },
    }
  }
}, {
  safeAreaInset: 'never',
  headerLayoutPreset: 'center',
  initialRouteName: "More",
});

export const HomeNavigator = createBottomTabNavigator({
  Indicadores: {
    screen: IndicadoresStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons name="tachometer" size={20} color={focused ? tintColor : 'grey'} />
      )
    }
  },
  Preguntas: {
    screen: PreguntasStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons name="comments" size={20} color={focused ? tintColor : 'grey'} />
      )
    }
  },
  Mensajes: {
    screen: MensajesStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons name="send-o" size={20} color={focused ? tintColor : 'grey'} />
      )
    }
  },
  Ventas: {
    screen: VentasStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionic name="ios-pricetag" size={20} color={focused ? tintColor : 'grey'} />
      )
    }
  },
  More: {
    screen: MoreStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) =>(
          <Icon name="dots-horizontal" size={35} color={focused ? tintColor : 'grey'} style={{ paddingTop: 10 }} />
        ),
      title: 'More'
    },
  },

},
  {
    tabBarOptions: {
      activeTintColor: Colors.blue,
    },
    initialRouteName: 'Indicadores',
  });

export const DrawerNavigator = createDrawerNavigator({
  Home: HomeNavigator,
},
  {
    order: ['Home'],
    initialRouteName: 'Home',
    drawerWidth: '80%',
    drawerPosition: 'right',
    mode: Platform.OS === 'ios' ? 'modal' : 'card',
    contentComponent: props => <Drawr {...props} />,
    useNativeAnimations: true,
    drawerBackgroundColor: 'white',
    contentOptions: {
      activeTintColor: 'white',
      activeBackgroundColor: 'orange',
      inactiveTintColor: 'white',
      itemsContainerStyle: {
        justifyContent: 'center',
      },
      itemStyle: {
        // ImageBackground:'red'
      },
      labelStyle: {
        fontFamily: 'normal',
        fontSize: 16
      },
      activeLabelStyle: {
        fontFamily: 'bold',
        fontSize: 16
      },
      inactiveLabelStyle: {
        fontFamily: 'normal',
        fontSize: 16
      },
      iconContainerStyle: {
        opacity: 1,
        // backgroundColor:'red'
      }
    }
  }
);
const LoginStack = createStackNavigator({
  Login,
}, {
  initialRouteName: "Login",
  headerMode: 'none',
});

const AppNavigation = createSwitchNavigator({
  Auth: {
    screen: LoginStack
  },
  App: {
    screen: DrawerNavigator
  }
},
  {
    initialRouteName: "Auth",
  }
);

export default createAppContainer(AppNavigation)
