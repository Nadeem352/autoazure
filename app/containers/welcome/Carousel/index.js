import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Dimensions from 'Dimensions';
import { Constants } from '@common';

const { width,height } = Constants.Window;

export default class CustomCarousel extends Component<{}> {


  state = {
    carouselIndex: 0,
  };

  data = [{
      'title': 0,
    }, {
      'title': 1,
    }, {
      'title': 2,
    }
  ];

    _renderItem ({item}) {

      if (item.title === 0) {
        return (
            <View style={styles.slide0.container}>
              <Text style={styles.slide0.text}> slider {item.title+1} </Text>
            </View>
        );
      }
      else if (item.title === 1) {
        return (
          <View style={styles.slide1.container}>
            <Text style={styles.slide1.text}> slider {item.title+1} </Text>
          </View>
        );
      } else if (item.title === 2) {
        return (
          <View style={styles.slide2.container}>
            <Text style={styles.slide2.text}> slider {item.title+1} </Text>
          </View>
        );
      }
    }

    async _handleSlideChange(index) {
       this.setState({carouselIndex: index});
    }

    render () {
        return (
          <View style={{flex:1}} >
            <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.data}
              renderItem={this._renderItem}
              // layout={'tinder'}
              bounces={false}
              sliderWidth={width-40}
              itemWidth={width-40}
              inactiveSlideOpacity={0}
              inactiveSlideScale={1}
              onBeforeSnapToItem={(index) => this._handleSlideChange(index)}/>
           { this.pagination }
          </View>
        );
    }

    get pagination () {
          return (
              <Pagination
                dotsLength={this.data.length}
                activeDotIndex={this.state.carouselIndex}
                containerStyle={styles.pagination}
                dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 10,
                  justifyContent:'center',
                  backgroundColor: '#ffffff'
                }}
                inactiveDotStyle={{
                  borderRadius: 10,
                  backgroundColor:  'rgba(255, 255, 255, .4)'
                }}
                inactiveDotOpacity={1}
                inactiveDotScale={1}
              />
          );
      }
}

const styles = {

  slide0: {
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent:'center',
      backgroundColor:'purple'
    },
    text: {
      fontSize:28
    }
  },
  slide1: {
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent:'center',
      backgroundColor:'green'
    },
    text: {
      fontSize:28
    }
  },
  slide2: {
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent:'center',
      backgroundColor:'orange'
    },
    text: {
      fontSize:28
    }
  },
  pagination: {
    position:'absolute',
    alignSelf:'center',
    paddingTop: height/3 - 40
  }
};
