import React, { Component } from 'react';
import {
  ImageBackground, KeyboardAvoidingView, ScrollView, View,
  Image, Text, TextInput, TouchableOpacity, ActivityIndicator
} from 'react-native';
import { styles } from '../styles/login';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { login, resetSate } from '../../redux/actions/login';
import AlertBox from '../../components/alertDialog';
import { Languages, Constants } from '@common';
const { behavior } = Constants.Window;
import LoadingButton from '../../components/loadingButton/index';
import { Button } from '../../components/buttons/buttons';
import { Colors } from '@common';
const { fontSize, Window } = Constants;
const { width } = Window;
class Login extends Component {
  static navigationOptions = {
    title: 'Login'
  };
  constructor(props) {
    super(props);
    this.state = {
      company: '', //'DEMO'
      name: '', //'Rizwan01',
      password: '', //'RIZWAN123',
    }
  }
  componentDidMount(){
    // console.log('props are', this.props);
    // if(this.props.loginData.Token){
    //   this.props.navigation.navigate('App');
    // }
  }
  componentWillReceiveProps(nextProps) {

    if (nextProps.status === 200) {
      this.props.navigation.navigate('App');
    } else if (nextProps.status === 500) {
      this.showAlert(nextProps.error);
      this.props.resetSate()
    }else if (nextProps.status === 401) {
      this.showAlert(nextProps.error);
      this.props.resetSate()

    }
  }
  login = async () => {
    if (this.state.company === '') {
      this.showAlert('Company is required')
    } else if (this.state.name === '') {
      this.showAlert('User Name is required')
    }else if (this.state.password === '') {
      this.showAlert('Password is required')
    } else {
      await this.props.login({ ...this.state });
    }
  }

  showAlert = (error) => {
    AlertBox({ title: 'Authentication', error: error })
  }

  render() {
    const { loading, status,error, loginData  } = this.props;

    return (
      <KeyboardAvoidingView
        style={styles.container}
        keyboardVerticalOffset={30}
        behavior={behavior}
        enabled>
        <ScrollView
          style={styles.mainView}
          bounces={false}
          contentContainerStyle={styles.grow}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'>
          <View style={styles.logoView}>
            <Image source={require('@assets/images/logo-with-name.jpg')} style={{ height: 200, width: 150, resizeMode: 'contain' }} />
          </View>
          <Text style={styles.heading}> {Languages.login.toUpperCase()} </Text>
          <View style={styles.spaceBetween}>
            <View style={styles.inputView}>
              <TextInput
                style={styles.textInput}
                onChangeText={(text) => this.setState({ company: text })}
                value={this.state.company}
                placeholder={Languages.company}
                placeholderTextColor={'#c4c4c4'}
                underlineColorAndroid={'transparent'}
                returnKeyType={Languages.next}
                onSubmitEditing={() => { this.username.focus(); }}
                ref={(c) => (this.username = c)}
              />
              <TextInput
                style={styles.textInput}
                onChangeText={(value) => this.setState({ name: value })}
                value={this.state.name}
                placeholder={Languages.username}
                placeholderTextColor={'#c4c4c4'}
                underlineColorAndroid={'transparent'}
                returnKeyType={Languages.next}
                onSubmitEditing={() => { this.password.focus(); }}
                ref={(c) => (this.username = c)}
              />
              <TextInput
                style={styles.textInput}
                onChangeText={(value) => this.setState({ password: value })}
                value={this.state.password}
                placeholder={Languages.password}
                placeholderTextColor={'#c4c4c4'}
                underlineColorAndroid={'transparent'}
                returnKeyType={Languages.done}
                secureTextEntry={true}
                ref={(c) => (this.password = c)}
              />
              {
                loading ? <ActivityIndicator style = {{marginTop:80}} size = 'small' /> :  <Button
                backgroundColor = { Colors.blue }
                borderColor= { Colors.blue }
                onPress = { this.login }
                style={{ marginBottom: 20 }}
                text = { 'LOG IN' }
                color = { 'white' }
                />

              }

            </View>

            {/* <LoadingButton isLoading = {loading} title={Languages.log_in} onPress={() => this.login()} /> */}

          </View>

        </ScrollView>

      </KeyboardAvoidingView>
    );
  }
}

// const styles = {
//   button: {
//     height: 50,
//     width: width - 40,
//     backgroundColor: Colors.blue,
//     borderRadius: 5,
//     justifyContent: 'center',
//     marginVertical:20,
//   },
//   title: {
//     letterSpacing: 2,
//     color: Colors.white,
//     fontSize: fontSize.large,
//   },
//   buttonView: {
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// };

const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({ login, resetSate }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login)
