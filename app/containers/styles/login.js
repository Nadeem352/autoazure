import { StyleSheet } from 'react-native'
import { Constants, Colors } from '@common';

const { fontSize, Window } = Constants;

export const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    mainView: {
      flex:1
    },
    grow: {
      flexGrow:1
    },
    logoView: {
      width:Window.width-40,
      alignItems:'center',
      justifyContent:'center',
      marginVertical:30
    },
    spaceBetween: {
      flex:1,
      justifyContent:'space-between'
    },
    heading: {
      fontSize: fontSize.large,
      color: Colors.blue
    },
    textInput:{
      fontSize:fontSize.medium,
      padding: 5,
      borderWidth: 2,
      fontSize: 19,
      color: 'black',
      width:Window.width-40,
      borderColor:Colors.blue,
      height:50,
      borderRadius:5,
      marginVertical:20
    },
    registerView:{
      alignItems:'center'
    },
    registerText: {
      fontSize:fontSize.extraLarge,
      color: Colors.white,
      textDecorationLine:'underline',
    }
})
