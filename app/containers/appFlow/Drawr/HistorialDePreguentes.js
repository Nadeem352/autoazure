import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import UserIcon from 'react-native-vector-icons/AntDesign';
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';
import MessageIcon from 'react-native-vector-icons/Feather';
import ClockIcon from 'react-native-vector-icons/AntDesign';




class HistorialDePreguentes extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        const { navigation } = this.props.navigation;
        return (

            <View style={styles.container1}>
                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <MessageIcon name='message-circle' size={20} color='#644244' style={styles.messageStyle} />
                        <Text style={styles.msgtext1}>Tienes para 2008 </Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: 13 }}>
                        <ClockIcon name='clockcircleo' size={14} color='black' style={styles.userIconStyle1} />
                        <Text style={styles.profileText1}> hace un dia </Text>
                    </View>
                </View>
                <View>
                    <View style={{ marginBottom: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <MessageIcon name='message-circle' size={20} color='#644244' style={styles.messageStyle} />
                            <Text style={styles.msgtext1}>Serla tan amable de proporcionarme el numero</Text>
                        </View>
                        <View>
                            <Text style={styles.msgtext}>VIN series de su vehiculo para poder identificar en el </Text>
                            <Text style={styles.msgtext}>catalago de planta la pleza por favor.</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginLeft: 13, marginTop: 3 }}>
                            <ClockIcon name='clockcircleo' size={14} color='black' style={styles.userIconStyle1} />
                            <Text style={styles.profileText1}> hace un dia </Text>
                        </View>
                    </View>
                </View>
            </View>

        );
    }
}
const mapStateToProps = state => {
    let data = state.loginData;
    return {
        loading: data.loading,
        status: data.status,
        error: data.error,
        loginData: data.data
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(HistorialDePreguentes))
