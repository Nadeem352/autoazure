import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';

import DrawrComponent from './DrawrComponent';

class Drawr extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  goToDetail = () => {
    this.props.navigation.navigate('HistorialDePreguentes')
  }
  render() {
    const { navigation } = this.props.navigation;
    return (
      <View style={styles.container}>
        <DrawrComponent text={'Insertar link a publicacion'} onPress={()=> {}}/>
        <DrawrComponent text={'Insertar costo de Mercado Envios'} onPress={()=>{}}/>
        <DrawrComponent 
          text={'Historial de preguntes'} 
          onPress={this.goToDetail}
          />
      </View>
    );
  }
}
const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Drawr))
