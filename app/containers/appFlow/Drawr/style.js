import { StyleSheet } from 'react-native';
import { Colors, Constants } from '@common';
const { fontSize, Window, Dimension } = Constants;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ///////////// Unread ///////////////
  itemContainer: {
    height: Dimension.ScreenHeight(0.08),
    width: Dimension.ScreenWidth(1),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.attributes.lightgrey,
  },
  Text:{
    color:Colors.Text, 
    fontSize: fontSize.small, 
    marginHorizontal: 15,
    // width: Dimension.ScreenWidth(0.45),
  },
  IconContView:{
    flexDirection: 'row'
  },
  IconsStyle:{
    marginRight: 20,
    marginVertical: 10
  },
  SwitchCon: {
    flex:1,
    alignItems:'flex-end'
  },
  Switch: {
    marginHorizontal: 10
  },
  messageStyle: {
    marginVertical: 3,
    marginTop:20,
    marginLeft:10
  },
  msgtext1:{
    fontSize:fontSize.small,
    marginTop:20,
    marginLeft:10,
    color:'black'
  },
  profileText1:{
    fontSize: fontSize.small,
    color:'grey',
    marginLeft:5,
    // lineHeight: 15
  },
  userIconStyle1: {
    marginVertical: 3,
  },
  msgtext:{
    fontSize: fontSize.small,
    color:'black',
    marginLeft:10
  },
  container1:{
    borderBottomWidth:0.5,
    borderBottomColor:'grey',
  },
  
  ///////////// Historial ///////////////

  //////////// Tabs ////////////////////
  performanceContainer: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 20
  },
  tabBar: {
    backgroundColor: Colors.offWhite
  },
  inactiveColor: {
    backgroundColor: Colors.blue
  },
});

export default styles;