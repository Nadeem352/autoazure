import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Switch } from 'react-native';
import styles from './style';
import UserIcon from 'react-native-vector-icons/AntDesign';
import SendIcon from 'react-native-vector-icons/FontAwesome';
import NotificationIcon from 'react-native-vector-icons/MaterialIcons';
import EyeIcon from 'react-native-vector-icons/Entypo';

class DrawrComponents extends Component {
  render() {
    const { key, onPress } = this.props;
    return (
      <TouchableOpacity key={key} style={styles.itemContainer} onPress={()=> onPress() }>
        <Text style={styles.Text}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

export default DrawrComponents;
