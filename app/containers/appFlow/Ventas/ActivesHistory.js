import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import ListItem from './ListItem';

class ActivesHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dumy: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }

  render() {

    return (
      <ScrollView style={styles.container}>
      {
        this.state.dumy.map((item, key) => {
          return (
            <ListItem key={key} item={item} />
          )
        })
      }
    </ScrollView>
    );
  }
}
const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ActivesHistory)
