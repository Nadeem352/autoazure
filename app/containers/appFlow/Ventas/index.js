import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Actives from './ActivesHistory';
import Closed from './ClosedHistory';
import FilterIcon from 'react-native-vector-icons/AntDesign';
import Unread from './Unread';
import { Colors, Languages } from '@common';
import styles from './style';

class Mensajes extends Component {
  static navigationOptions = {
    headerRight: (        
            <FilterIcon name='filter' color='white' size={24} />
    )
}
  state = {
      index: 0,
      routes: [
        { key: 'Unread', title: Languages.unread },
        { key: 'Actives', title: Languages.actives },
        { key: 'Closed', title: Languages.closed },
      ],
    };

  _renderTabBar = (props) => <TabBar
    style={styles.tabBar}
    indicatorStyle={styles.inactiveColor}
    activeColor={Colors.blue}
    inactiveColor={Colors.lightBlue}
    pressOpacity={1}
    {...props} />;

  render() {

    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({ Actives, Closed, Unread })}
        onIndexChange={(index) => this.setState({ index })}
        renderTabBar={this._renderTabBar}
      />
    );
  }
}

const mapStateToProps = state => {
  return {

  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Mensajes)
