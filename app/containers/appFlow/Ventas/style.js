import { StyleSheet } from 'react-native';
import { Colors, Constants } from '@common';
const { fontSize, Window, Dimension } = Constants;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ///////////// Unread ///////////////
  itemContainer: {
    height: Dimension.ScreenHeight(0.22),
    width: Dimension.ScreenWidth(1),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.6,
    borderBottomColor: Colors.attributes.black,
  },
  dateTextCon:{
    fontSize: fontSize.small,
    color:'grey'
  },
  itemImageCon: {
    height: Dimension.ScreenHeight(0.17),
    width: Dimension.ScreenWidth(0.33),
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    height: Dimension.ScreenHeight(0.17),
    width: Dimension.ScreenWidth(0.28),
    resizeMode:'contain'
  },
  detailContainer: {
    height: Dimension.ScreenHeight(0.15),
    width: Dimension.ScreenWidth(0.64),
    marginHorizontal: 3,
    justifyContent: 'center',
  },
  title: {
    fontSize: fontSize.normal,
    marginTop: 3,
  },
  profileView: {
    height: Dimension.ScreenHeight(0.030),
    width: Dimension.ScreenWidth(0.67),
    flexDirection: 'row'
  },
  userIconStyle1: {
    marginVertical: 3,
  },
  profileText1: {
    fontSize: fontSize.small,
    marginHorizontal: 5,
    marginTop: 2,
    color:'grey'
  },
  userIconStyle2: {
    marginVertical: 3,
  },
  profileText2: {
    fontSize: fontSize.small,
    color:'grey',
    marginTop: 1,
  },
  detalis: {
    fontSize: fontSize.small,
    color:'grey',
    width: Dimension.ScreenWidth(0.62)
  },
  ///////////// Historial ///////////////

  //////////// Tabs ////////////////////
  performanceContainer: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 20
  },
  tabBar: {
    backgroundColor: Colors.offWhite
  },
  inactiveColor: {
    backgroundColor: Colors.blue
  },
});

export default styles;