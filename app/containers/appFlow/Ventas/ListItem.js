import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import styles from './style';
import UserIcon from 'react-native-vector-icons/Entypo';

class ListItem extends Component {

  render() {
    const { key, item } = this.props;
    return (
      <TouchableOpacity key={key} style={styles.itemContainer} >
        <View style={styles.itemImageCon}>
          <Image source={require('@assets/images/logo-with-name.jpg')} style={styles.imageStyle}></Image>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.dateTextCon}>27/06/2019 12:36</Text>
          <Text style={styles.title}>Set 8 Popote Pajilla Metal Acer...</Text>
          <View style={styles.profileView}>
            <Text style={styles.profileText2}>$178 * unidad </Text>
          </View>
          <View style={styles.profileView}>
            <UserIcon name='user' size={16} color='black' style={styles.userIconStyle1} />
            <Text style={styles.profileText1}>Danna Zubiri</Text>
          </View>
          <Text style={styles.detalis}>DANNAZUBIRI (1 punto)</Text>
          <Text style={styles.detalis}>Operacion # 2068034706</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ListItem;
