import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';
import UserIcon from 'react-native-vector-icons/Entypo';
import ClockIcon from 'react-native-vector-icons/AntDesign';

import { Images } from '@common';
const { images } = Images;

class Historial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dumy: [{},{},{},{},{},{},{},{},{},{},{}]
    }
  }

  renderItem = (item,key) => {
    return (
      <TouchableOpacity key={key} style={styles.itemContainer}
      onPress={()=>this.props.navigation.navigate('DetaliScreen')}>
        <View style={styles.itemImageCon}>
          <Image source={require('@assets/images/logo-with-name.jpg')} style={styles.imageStyle}></Image>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.title}>Soport Faro Izqulerdo Sonic 1!...</Text>
          <View style={styles.profileView}>
            <UserIcon name='user' size={16} color='black' style={styles.userIconStyle1} />
            <Text style={styles.profileText1}> RUO5740024 </Text>
          </View>
          <View style={styles.profileView}>
            <ClockIcon name='clockcircleo' size={14} color='black' style={styles.userIconStyle2} />
            <Text style={styles.profileText2}> hace un dia </Text>
          </View>
          <Text style={styles.detalis}>buenos dias tienes en existenciael ferlo</Text>
        </View>
      </TouchableOpacity>
    )
  }

  render() {

    return (
      <ScrollView style={styles.container}>
        {
          this.state.dumy.map((item,key)=>{
            return(
              this.renderItem(item,key)
            )
          })
        }
      </ScrollView>
    );
  }
}
const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Historial))