import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import DollarIcon from 'react-native-vector-icons/FontAwesome';
import TruckIcon from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/AntDesign';
import MessageIcon from 'react-native-vector-icons/Feather';
import UserIcon from 'react-native-vector-icons/Entypo';
import Calendar from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements';


class DetaliScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false
        }
    }


    render() {

        return (
            <ScrollView style={styles.container}>
                <View style={styles.detailScrn}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.detailImageCon}>
                            <Image source={require('@assets/images/logo-with-name.jpg')} style={styles.detailimageStyle}></Image>
                        </View>
                        <View style={styles.detailCont}>
                            <Text style={styles.detailTitle}>Mangueta Direccion Tornado 1.61 2011-2019</Text>
                            <Text style={styles.detailText}>$ 3.372 x 20 disponibles</Text>
                            <View style={styles.detailIconView}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Icon name='dropbox' size={20} color='#6b88a8' style={styles.dropBoxStyle} />
                                    <TruckIcon name='truck' size={20} color='#13431b' style={styles.truckStyle} />
                                    <DollarIcon name='dollar' size={20} color='#88656c' style={styles.dollarStyle} />
                                    <MessageIcon name='message-circle' size={20} color='#644244' style={styles.messageStyle} />

                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.profile}>
                    <View style={styles.profileDetail}>
                        <UserIcon name='user' size={16} color='black' style={styles.userIconStyle} />
                        <Text style={styles.profileText}> DALIAMARISELAALANIZCARDENAS (0 PUNTOS) </Text>
                    </View>
                    <View style={styles.profileDetail1}>
                        <Calendar name='calendar-check-o' size={16} color='black' style={styles.CalendarStyle} />
                        <Text style={styles.profileText1}> Miembro desde: 28/05/2018</Text>
                    </View>
                </View>
                <View style={styles.textView}>
                    <Text style={styles.textText}> 93cxm80298c109219</Text>
                </View>
                <View style={styles.textInputView}>
                    <TextInput
                        placeholder='Ingresa tu respuesta...'
                        placeholderTextColor={'grey'}
                        style={styles.textInput}
                    />
                </View>
                <View style={styles.textView1}>
                    <View style={{ flexDirection: 'row' }}>
                        <CheckBox
                            title='Saludos y Garcias , GM Official'
                            checked={this.state.checked} onPress={() => this.setState({ checked: !this.state.checked })}
                            containerStyle={{ backgroundColor: 'transparent', marginVertical: 0, marginBottom: 0, borderWidth: 0, }}
                            textStyle={{ fontSize: 16, fontWeight: 'normal' }}
                        />
                    </View>
                    <View style={styles.textText2}>
                        <Text style={styles.textText1}> 1970/2000</Text>
                    </View>
                </View>
                <View style={styles.textView2}>
                    <Text style={styles.textText3}>Atajos de teclado: tipea '@' para respuestas rapidas o '#' para insertar un link a otra publiccion</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={styles.RespuestasView} onPress={()=>this.props.navigation.navigate('RespuestasRapidas')}>
                        <Text style={styles.TouchableOpacityTextView}>Respuestas Rapidas</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.EnviarrespuestaView}>
                        <Text style={styles.TouchableOpacityTextView}>Enviar respuesta</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
const mapStateToProps = state => {
    let data = state.loginData;
    return {
        loading: data.loading,
        status: data.status,
        error: data.error,
        loginData: data.data
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DetaliScreen)
