import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, Switch } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';
import PencilIcon from 'react-native-vector-icons/EvilIcons';
import AddIcon from 'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select';

import { Images } from '@common';
import { colors } from 'react-native-elements';
const { images } = Images;

class RespuestasRapidas extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    static navigationOptions = {
        headerRight: (
            <View style={{ height: 50, width: 70, justifyContent:'center',alignItems:'center',flexDirection:'row' }}>
                <AddIcon name='ios-add' color='white' size={30} />
                <View style={{ width: 10 }} />
                <PencilIcon name='pencil' color='white' size={30} />
            </View>
        )
    }
    render() {

        return (
            <View style={styles.container}>
                <ScrollView>
                    <TouchableOpacity style={[styles.RespuestasRapidasContainer, { backgroundColor: 'blue' }]} >
                        <Text style={styles.RespuestasRapidasTextView}> HAY STOCK </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.RespuestasRapidasContainer, { backgroundColor: 'orange' }]} >
                        <Text style={styles.RespuestasRapidasTextView}> ENVIOS </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.RespuestasRapidasContainer, { backgroundColor: 'red' }]} >
                        <Text style={styles.RespuestasRapidasTextView}> SALUDOS </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.RespuestasRapidasContainer, { backgroundColor: 'green' }]} >
                        <Text style={styles.RespuestasRapidasTextView}> ENTREGAS </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.RespuestasRapidasContainer, { backgroundColor: 'pink' }]} >
                        <Text style={styles.RespuestasRapidasTextView}> HORARIO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.RespuestasRapidasContainer, { backgroundColor: 'grey' }]} >
                        <Text style={styles.RespuestasRapidasTextView}> NUMERO VIN</Text>
                    </TouchableOpacity>
                    <View style={styles.buttonView}>
                        <Text style={styles.bottonText}> NUMERO VIN</Text>
                        <Switch
                            style={{  }}
                            value={true}
                            disabled={false}
                            ios_backgroundColor='rgba(30, 165, 233, 1)'
                            trackColor='rgba(30, 165, 233, 1)'
                            thumbColor='#fff'
                            onChange={() => { }}
                            onValueChange={() => { }}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const mapStateToProps = state => {
    let data = state.loginData;
    return {
        loading: data.loading,
        status: data.status,
        error: data.error,
        loginData: data.data
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(RespuestasRapidas))
