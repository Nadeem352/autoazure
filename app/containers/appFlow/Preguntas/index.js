import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Activas from './Activas';
import Historial from './Historial';
import MenuIcon from 'react-native-vector-icons/Entypo';
import TagIcon from 'react-native-vector-icons/EvilIcons';
import { Colors, Languages } from '@common';
import styles from './style';

class Preguntas extends Component {
  static navigationOptions =({navigation})=>({
    headerRight: (
        <View style={{ height: 50, width: 70, justifyContent:'center',alignItems:'center',flexDirection:'row' }}>
            <TagIcon name='tag' color='white' size={30} />
            <View style={{ width: 10 }} />
            <TouchableOpacity onPress={()=> navigation.openDrawer()}>
              <MenuIcon name='menu' color='white' size={30}/>
            </TouchableOpacity>
        </View>
    )
  })
  state = {
      index: 0,
      routes: [
        { key: 'Activas', title: Languages.activas },
        { key: 'Historial', title: Languages.historial },
      ],
    };

  _renderTabBar = (props) => <TabBar
    style={styles.tabBar}
    indicatorStyle={styles.inactiveColor}
    activeColor={Colors.blue}
    inactiveColor={Colors.lightBlue}
    pressOpacity={1}
    {...props} />;

  render() {

    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({ Activas, Historial })}
        onIndexChange={(index) => this.setState({ index })}
        renderTabBar={this._renderTabBar}
      />
    );
  }
}

const mapStateToProps = state => {
  return {

  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Preguntas)
