import { StyleSheet } from 'react-native';
import { Colors, Constants } from '@common';
const { fontSize, Window, Dimension } = Constants;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ///////////// Ativas ///////////////
  itemContainer: {
    height: Dimension.ScreenHeight(0.18),
    width: Dimension.ScreenWidth(1),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.6,
    borderBottomColor: Colors.attributes.black,
  },
  itemImageCon: {
    height: Dimension.ScreenHeight(0.15),
    width: Dimension.ScreenWidth(0.33),
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    height: Dimension.ScreenHeight(0.08),
    width: Dimension.ScreenWidth(0.20)
  },
  detailContainer: {
    height: Dimension.ScreenHeight(0.15),
    width: Dimension.ScreenWidth(0.64),
    marginHorizontal: 10,
    justifyContent: 'center',
  },
  title: {
    fontSize: fontSize.normal,
    marginTop: 3,
  },
  profileView: {
    height: Dimension.ScreenHeight(0.032),
    width: Dimension.ScreenWidth(0.67),
    flexDirection: 'row'
  },
  userIconStyle1: {
    marginVertical: 3,
  },
  profileText1: {
    fontSize: fontSize.small,
    fontStyle: 'normal',
    fontWeight: 'normal',
    marginHorizontal: 5,
    marginTop: 4,
  },
  userIconStyle2: {
    marginVertical: 3,
  },
  profileText2: {
    fontSize: fontSize.small,
    color: 'grey',
    marginTop: 1,
    marginHorizontal: 5,
  },
  detalis: {
    fontSize: fontSize.small,
    color: 'grey',
    width: Dimension.ScreenWidth(0.62)
  },
  ///////////// DetailScreen ///////////////
  detailScrn: {
    height: Dimension.ScreenHeight(0.18),
    width: Dimension.ScreenWidth(1),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  detailImageCon: {
    height: Dimension.ScreenHeight(0.16),
    width: Dimension.ScreenWidth(0.30),
    alignItems: 'center',
    justifyContent: 'center',
  },
  detailimageStyle: {
    height: Dimension.ScreenHeight(0.13),
    width: Dimension.ScreenWidth(0.25)
  },
  detailCont: {
    height: Dimension.ScreenHeight(0.16),
    width: Dimension.ScreenWidth(0.70),
    marginHorizontal: 5,

  },
  detailTitle: {
    fontSize: fontSize.normal,
    marginTop: 3,
    fontWeight: 'normal',
    textDecorationLine: 'underline',
    lineHeight: 24
  },
  detailText: {
    fontSize: fontSize.small,
    color: 'grey',
    marginTop: 3
  },
  detailIconView: {
    height: Dimension.ScreenHeight(0.06),
    width: Dimension.ScreenWidth(0.70),
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  dropBoxStyle: {
    marginVertical: 3,
  },
  truckStyle: {
    marginVertical: 3,
    marginHorizontal: 10
  },
  dollarStyle: {
    marginVertical: 3,
    marginRight: 10
  },
  messageStyle: {
    marginVertical: 3,
  },
  profile: {
    height: Dimension.ScreenHeight(0.1),
    width: Dimension.ScreenWidth(0.92),
    backgroundColor: Colors.lightGrey,
    marginHorizontal: 20,
    marginTop: 10
  },
  profileDetail: {
    marginTop: 15,
    marginHorizontal: 10,
    flexDirection: 'row',
  },
  profileDetail1: {
    marginTop: 5,
    marginHorizontal: 10,
    flexDirection: 'row',
  },
  CalendarStyle: {
    marginVertical: 5,
  },
  textView: {
    height: Dimension.ScreenHeight(0.06),
    width: Dimension.ScreenWidth(0.92),
    backgroundColor: '#fff',
    marginHorizontal: 20,
    marginTop: 12,
    borderBottomColor: '#095548',
    borderBottomWidth: 2,
  },
  textText: {
    fontSize: fontSize.medium,
    marginHorizontal: 5,
    marginVertical: 10
  },
  textInputView: {
    height: Dimension.ScreenHeight(0.15),
    width: Dimension.ScreenWidth(0.92),
    backgroundColor: Colors.lightGrey,
    marginHorizontal: 20,
    marginTop: 20
  },
  textInput: {
    fontSize: fontSize.medium,
    marginHorizontal: 5,
    marginVertical: 2,
  },
  textView1: {
    height: Dimension.ScreenHeight(0.10),
    width: Dimension.ScreenWidth(0.92),
    backgroundColor: '#fff',
    marginHorizontal: 20,
    marginTop: 10,
    borderBottomColor: '#095548',
    borderBottomWidth: 2,
  },
  textViewButton: {
    height: Dimension.ScreenHeight(0.05),
    width: Dimension.ScreenWidth(0.10),
    backgroundColor: 'blue',
    borderRadius: 100,
    backgroundColor: '#156a5b'
  },
  textText1: {
    fontSize: fontSize.small,
    marginTop: 8,
    backgroundColor: '#fff'

  },
  textText2: {
    fontSize: fontSize.smalll,
    alignItems: 'flex-end'
  },
  textView2: {
    height: Dimension.ScreenHeight(0.07),
    width: Dimension.ScreenWidth(0.92),
    backgroundColor: '#fff',
    marginHorizontal: 20,
    marginTop: 10,
  },
  textText3: {
    fontSize: fontSize.small,
    marginHorizontal: 5,
    marginVertical: 5
  },
  EnviarrespuestaView: {
    height: Dimension.ScreenHeight(0.07),
    width: Dimension.ScreenWidth(0.45),
    backgroundColor: Colors.blue1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    // marginTop:20,
    borderRadius: 10,

  },
  TouchableOpacityTextView: {
    fontSize: 16,
    color: '#fff',
    alignSelf: 'center',
  },
  RespuestasView: {
    height: Dimension.ScreenHeight(0.07),
    width: Dimension.ScreenWidth(0.45),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.blue1,
    marginHorizontal: 10,
    // marginTop:20,
    borderRadius: 10,
  },
  RespuestasRapidasContainer: {
    height: Dimension.ScreenHeight(0.07),
    width: Dimension.ScreenWidth(.90),
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 0.1,
    justifyContent: 'center',
    alignContent: 'center',
    borderColor: Colors.attributes.black,
    backgroundColor: Colors.blue,
    marginTop: 20,
    borderRadius: 4
  },
  RespuestasRapidasTextView: {
    fontSize: fontSize.medium,
    color: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
    // marginVertical: 14,
  },
  buttonView:
  {
    height: 60,
    width: Dimension.ScreenWidth(1),
    alignItems:'center',
    backgroundColor: 'lightgrey',
    marginTop: Dimension.ScreenHeight(0.19),
    flexDirection: 'row'
  },
  bottonText: {
    fontSize: fontSize.small,
    width: Dimension.ScreenWidth(0.82),
    marginLeft: Dimension.ScreenWidth(0.03),
    textAlign: 'left',
  },
  ///////////// Historial ///////////////

  //////////// Tabs ////////////////////
  performanceContainer: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 20
  },
  tabBar: {
    backgroundColor: Colors.offWhite
  },
  inactiveColor: {
    backgroundColor: Colors.blue
  },
});

export default styles;
