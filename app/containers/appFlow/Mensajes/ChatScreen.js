import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, TextInput, Dimensions } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import DollarIcon from 'react-native-vector-icons/FontAwesome';
import TruckIcon from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/AntDesign';
import MessageIcon from 'react-native-vector-icons/Feather';
import UserIcon from 'react-native-vector-icons/Entypo';
import Calendar from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements';
import ClockIcon from 'react-native-vector-icons/AntDesign';
import camera from 'react-native-vector-icons/Feather';
import AttachmentIcon from 'react-native-vector-icons/Entypo';
import SendIcon from 'react-native-vector-icons/FontAwesome';


import { placeholder } from '@babel/types';



class ChatScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dumy: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
        }
    }

    renderChatItem = (item,key) => {
        return (
            <View key={key} style={styles.container}>
                <View style={styles.chat1}>
                    <Text style={styles.chat1Text}>GM Oficial,</Text>
                </View>
                <View style={styles.timeView1}>
                    <ClockIcon name='clockcircleo' size={10} color='grey' style={styles.userIconStyle1} />
                    <Text style={styles.msgDate}> hace 06 horas </Text>
                </View>
                <View style={styles.chat2}>
                    <Text style={styles.chat2Text}>Listo Pablo, ya la compre viene de acuerdo a la serie verded.???</Text>
                </View>
                <View style={styles.timeView2}>
                    <ClockIcon name='clockcircleo' size={10} color='grey' style={styles.userIconStyle1} />
                    <Text style={styles.msgDate2}> hace 06 horas </Text>
                </View>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.detailScrn}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.detailImageCon}>
                            <Image source={require('@assets/images/logo-with-name.jpg')} style={styles.detailimageStyle}></Image>
                        </View>
                        <View style={styles.detailCont}>
                            <View>
                                <Text style={styles.detailTitle}>Manguera Para Clima Conden...</Text>
                            </View>
                            <View style={styles.profileDetail1}>
                                <Calendar name='calendar-check-o' size={16} color='black' style={styles.CalendarStyle} />
                                <Text style={styles.profileText}> 20/06/2019 4:50PM</Text>
                            </View>
                            <View style={styles.detailIconView}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Icon name='dropbox' size={20} color='#6b88a8' style={styles.dropBoxStyle} />
                                    <TruckIcon name='truck' size={20} color='#13431b' style={styles.truckStyle} />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.textView}>
                    <Text style={styles.text}> ver mas informacion</Text>
                </View>
                <ScrollView>
                    {
                        this.state.dumy.map((item, key) => {
                            return (
                                this.renderChatItem(item,key)
                            )

                        })
                    }
                </ScrollView>
                <View style={styles.textInputContainer}>
                    <Icon name='camera' size={40} color='white' style={styles.textInputViewCamera} />
                    <View style={styles.textInputView}>
                        <TextInput
                            placeholder='Ingresa tu mensaje'
                            style={styles.textStyle}
                        />
                        <AttachmentIcon name='attachment' size={22} color='lightgrey' style={styles.attachment} />
                        <SendIcon name='send-o' size={25} color='lightgrey' style={styles.SendIcon} />
                    </View>
                </View>
            </View>
        );
    }
}
const mapStateToProps = state => {
    let data = state.loginData;
    return {
        loading: data.loading,
        status: data.status,
        error: data.error,
        loginData: data.data
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen)