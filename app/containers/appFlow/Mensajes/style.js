import { StyleSheet } from 'react-native';
// import { Dimensions } from 'react-native';
import { Colors, Constants } from '@common';
const { fontSize, Window, Dimension } = Constants;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  
  ///////////// Mensajes ///////////////
 
  itemContainer: {
    height: Dimension.ScreenHeight(0.16),
    width: Dimension.ScreenWidth(1),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.6,
    borderBottomColor: Colors.attributes.black,
  },
  itemImageCon: {
    height: Dimension.ScreenHeight(0.15),
    width: Dimension.ScreenWidth(0.30),
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    height: Dimension.ScreenHeight(0.08),
    width: Dimension.ScreenWidth(0.20)
  },
  detailContainer: {
    height: Dimension.ScreenHeight(0.13),
    width: Dimension.ScreenWidth(0.67),
    marginHorizontal: 10,
    justifyContent: 'center',
  },
  textViewButton:{
    height:Dimension.ScreenHeight(0.01),
    width:Dimension.ScreenWidth(0.02),
    borderRadius:100,
    backgroundColor:'red',
    marginTop:11,
  },
  textViewButton1:{
    height:Dimension.ScreenHeight(0.01),
    width:Dimension.ScreenWidth(0.02),
    borderRadius:100,
    backgroundColor:'grey',
    marginTop:8,
  },
  
  textText1: {
    fontSize: fontSize.small,
    marginTop:8,
    backgroundColor:'#fff'
  
  },
  title: {
    fontSize: fontSize.normal,
    marginTop: 3,
  },
  profileView: {
    height: Dimension.ScreenHeight(0.035),
    width: Dimension.ScreenWidth(0.67),
    flexDirection: 'row'
  },
  userIconStyle1: {
    marginVertical: 3,
  },
  profileText1: {
    fontSize: fontSize.small,
    fontStyle: 'normal',
    fontWeight: 'normal',
    marginHorizontal: 5,
    marginTop: 4,
  },
  userIconStyle2: {
    marginVertical: 3,
  },
  profileText2: {
    fontSize: fontSize.small,
    color:'grey',
    marginTop: 1,
    marginHorizontal: 5,
  },
  detalis: {
    fontSize: fontSize.small,
    color:'grey',
    width: Dimension.ScreenWidth(0.62)
  },
  togleView:{
  height: Dimension.ScreenHeight(0.07),
  width: Dimension.ScreenWidth(1),
  backgroundColor:'white',
  flexDirection:'row',
  borderBottomWidth:0.5,
  borderBottomColor:Colors.attributes.black,
},
togleText:{
  fontSize:fontSize.large,
  marginLeft:250,
  marginTop:11,
  marginRight:200,
  color:'black'
},
  
  ///////////// ChatScreen ///////////////
  detailScrn: {
    height: Dimension.ScreenHeight(0.18),
    width: Dimension.ScreenWidth(1),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor:'#fff'
  },
  detailImageCon: {
    height: Dimension.ScreenHeight(0.16),
    width: Dimension.ScreenWidth(0.30),
    alignItems: 'center',
    justifyContent: 'center',
  },
  detailimageStyle: {
    height: Dimension.ScreenHeight(0.13),
    width: Dimension.ScreenWidth(0.25),
  },
  detailCont: {
    height: Dimension.ScreenHeight(0.16),
    width: Dimension.ScreenWidth(0.70),
    marginHorizontal:5,
  },
  detailTitle: {
    fontSize: fontSize.normal,
    marginTop: 3,
    fontWeight:'bold',
    textDecorationLine: 'underline',
    lineHeight:24
  },
  profileDetail1:{
    marginTop: 5,
    flexDirection:'row',
  },
  CalendarStyle: {
    marginVertical: 5,
    color:'grey'
  },
  profileText:{
    fontSize:fontSize.short,
    marginTop:4,
    color:'grey'
  },
detailIconView:{
    height:Dimension.ScreenHeight(0.06),
    width:Dimension.ScreenWidth(0.70),
    backgroundColor:'#fff',
    justifyContent:'center'
  },
  dropBoxStyle: {
    marginVertical: 3,
  },
  truckStyle: {
    marginVertical: 3,
    marginHorizontal:10
  },
  textView:{
    height:Dimension.ScreenHeight(0.06),
    width:Dimension.ScreenWidth(0.92),
    backgroundColor:'#fff',
    marginHorizontal:20,
    borderRadius:5
  },
  text:{
    fontSize:fontSize.small,
    textAlign:'center',
    color:'grey',
    marginVertical:10
  },
  chat1:{
    height:Dimension.ScreenHeight(0.09),
    width:Dimension.ScreenWidth(0.92),
    backgroundColor:'#227174',
    marginHorizontal:20,
    marginTop:20,
    borderRadius:5,
    justifyContent:'center'
  },
  chat1Text:{
    fontSize:fontSize.small,
    color:'white',
    textAlign: 'right',
    marginHorizontal: 10
  },
  timeView1: {
    height: Dimension.ScreenHeight(0.030),
    width: Dimension.ScreenWidth(0.95),
    flexDirection: 'row',
    alignSelf:'center',
    marginTop:2,
    backgroundColor:'#fff',
    justifyContent: 'flex-end'
  },
  userIconStyle1: {
    marginVertical: 3,
  },
  msgDate: {
    fontSize:10,
    color:'grey',
    marginHorizontal: 5,
  },
  chat2:{
    height:Dimension.ScreenHeight(0.09),
    width:Dimension.ScreenWidth(0.92),
    backgroundColor:'#bfb9bf',
    marginHorizontal:20,
    justifyContent: 'center',
    marginTop:20,
    borderRadius:5
  },
  chat2Text:{
    fontSize:fontSize.small,
    color:'white',
    marginHorizontal: 10
  },
  timeView2: {
    height: Dimension.ScreenHeight(0.030),
    width: Dimension.ScreenWidth(0.67),
    flexDirection: 'row',
    marginLeft:20,
    marginTop:2,
    backgroundColor:'#fff'
  },
  userIconStyle2: {
    marginVertical: 3,
  },
  msgDate2: {
    fontSize:10,
    color:'grey',
    marginHorizontal: 5,
  },
  textInputContainer:{
    height:Dimension.ScreenHeight(0.10),
    width:Dimension.ScreenWidth(1),
    backgroundColor:Colors.DirtyBackground,
    flexDirection:'row'
  },
  textInputViewCamera:{
    marginHorizontal:10,
    marginVertical:18

  },
  textInputView:{
    height:Dimension.ScreenHeight(0.08),
    width:Dimension.ScreenWidth(0.83),
    backgroundColor:'#fff',
    alignSelf:'center',
    borderWidth:1,
    borderRadius:20,
    borderColor:'grey',
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
  },
  textStyle:{
    fontSize:fontSize.medium,
    marginLeft:10,
    height: '100%',
    width: '65%',
    fontSize: 14,
  },
  attachment:{
    marginLeft: 20
  },
  SendIcon:{
    marginHorizontal:10
  },
  //////////// Tabs ////////////////////
  performanceContainer: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 20
  },
  tabBar: {
    backgroundColor: Colors.offWhite
  },
  inactiveColor: {
    backgroundColor: Colors.blue
  },
});

export default styles;