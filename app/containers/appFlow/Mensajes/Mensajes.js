import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView,Button, Dimensions ,Switch} from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import UserIcon from 'react-native-vector-icons/Entypo';
import ClockIcon from 'react-native-vector-icons/AntDesign';

class Mensajes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dumy: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
renderItem = (item, key) => {  
    return (
        <TouchableOpacity key={key} style={styles.itemContainer}
        onPress={()=>this.props.navigation.navigate('ChatScreen')}>
          <View style={styles.itemImageCon}>
            <Image source={require('@assets/images/logo-with-name.jpg')} style={styles.imageStyle}></Image>
          </View>
          <View style={styles.detailContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={styles.textViewButton}></View>
              <Text style={styles.title}>  Set 3 Molduras Rueda Trax 1.6...</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={styles.textViewButton1}></View>
              <Text style={styles.title}>  No tengo noticias de</Text>
            </View>
            <View style={styles.profileView}>
              <UserIcon name='user' size={16} color='black' style={styles.userIconStyle1} />
              <Text style={styles.profileText1}> RUO5740024 </Text>
            </View>
            <View style={styles.profileView}>
              <ClockIcon name='clockcircleo' size={15} color='black' style={styles.userIconStyle2} />
              <Text style={styles.profileText2}> hace 06 horas </Text>
            </View>
          </View>
        </TouchableOpacity>
    )
  }

  render() {
    return (
      <ScrollView style={styles.container}>
              <View style={styles.togleView}>
              <Text style={styles.togleText}>No leidos</Text>
              <Switch
                style={{height:55,}}
                value={true}
                disabled={false}
                ios_backgroundColor='rgba(30, 165, 233, 1)'
                trackColor='rgba(30, 165, 233, 1)'
                thumbColor='#fff'
                onChange={() => { }}
                onValueChange={() => { }}
              />
              </View>

        {
          this.state.dumy.map((item, key) => {
            return (
              this.renderItem(item, key)
            )
          })
        }
      </ScrollView>
    );
  }
}
const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Mensajes)
