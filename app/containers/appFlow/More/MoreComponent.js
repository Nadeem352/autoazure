import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Switch } from 'react-native';
import styles from './style';
import UserIcon from 'react-native-vector-icons/AntDesign';
import SendIcon from 'react-native-vector-icons/FontAwesome';
import NotificationIcon from 'react-native-vector-icons/MaterialIcons';
import EyeIcon from 'react-native-vector-icons/Entypo';

class MoreComponents extends Component {

  render() {
    const { key, item, isIcon, isSwitch } = this.props;
    return (
      <TouchableOpacity key={key} style={styles.itemContainer} >
        <Text style={styles.Text}>COMPANIES2018</Text>
        {
          isIcon ?
            <View style={styles.IconContView}>
              <View>
                <UserIcon name='message1' size={24} color='lightgrey' style={styles.IconsStyle} />
                <View style={styles.badgeCon}>
                  <Text style={styles.badgeTxt}>550</Text>
                </View>
              </View>
              <View>
                <SendIcon name='send-o' size={24} color='lightgrey' style={styles.IconsStyle} />
                <View style={styles.badgeCon}>
                  <Text style={styles.badgeTxt}>194</Text>
                </View>
              </View>
              <View>
                <EyeIcon name='eye' size={24} color='lightgrey' style={styles.IconsStyle} />
                <View style={styles.badgeCon}>
                  <Text style={styles.badgeTxt}>200</Text>
                </View>
              </View>
              <View>
                <NotificationIcon name='notifications-none' size={24} color='lightgrey' style={styles.IconsStyle} />
                <View style={styles.badgeCon}>
                  <Text style={styles.badgeTxt}>156</Text>
                </View>
              </View>
            </View>
            :
            <View style={styles.SwitchCon}>
              <Switch
                style={styles.Switch}
                value={true}
                disabled={false}
                ios_backgroundColor='rgba(30, 165, 233, 1)'
                trackColor='rgba(30, 165, 233, 1)'
                thumbColor='#fff'
                onChange={() => { }}
                onValueChange={() => { }}
              />
            </View>
        }
      </TouchableOpacity>
    );
  }
}

export default MoreComponents;
