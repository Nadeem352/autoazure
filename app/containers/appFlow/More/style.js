import { StyleSheet } from 'react-native';
import { Colors, Constants } from '@common';
const { fontSize, Window, Dimension } = Constants;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ///////////// Unread ///////////////
  itemContainer: {
    height: Dimension.ScreenHeight(0.10),
    width: Dimension.ScreenWidth(1),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.attributes.lightgrey,
  },
  Text:{
    color:Colors.Text, 
    fontSize: fontSize.medium, 
    marginHorizontal: 15,
    width: Dimension.ScreenWidth(0.45),
  },
  IconContView:{
    flexDirection: 'row'
  },
  IconsStyle:{
    marginRight: 20,
    marginVertical: 10
  },
  SwitchCon: {
    flex:1,
    alignItems:'flex-end',
  },
  Switch: {
    marginHorizontal: 10
  },
  badgeCon: { height: 18, backgroundColor: 'red', position: 'absolute', borderRadius: 20, alignSelf: 'flex-end', alignItems: 'center', justifyContent: 'center', },
  badgeTxt: { fontSize: 10, color: '#fff', marginHorizontal: 7 },
  ///////////// Historial ///////////////

  //////////// Tabs ////////////////////
  performanceContainer: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 20
  },
  tabBar: {
    backgroundColor: Colors.offWhite
  },
  inactiveColor: {
    backgroundColor: Colors.blue
  },
});

export default styles;