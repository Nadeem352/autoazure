import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import styles from './style';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';

import MoreComponent from './MoreComponent';

class More extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [{},{},{},{},{},{},{}]
    }
  }

  render() {
    const { navigation } = this.props.navigation;
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
        { 
          this.state.data.map((item,key)=>{
            return(
              <MoreComponent key={key} isSwitch={false} isIcon={true} />
            )
          })
        }
         { 
          this.state.data.map((item,key)=>{
            return(
              <MoreComponent key={key} isIcon={false} isSwitch={true} />
            )
          })
        }
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(More))
