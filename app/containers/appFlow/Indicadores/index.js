import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import General from './General';
import Performance from './Performance';
import Graphs from './Graphs';
import { Colors, Languages } from '@common';
import styles from './style';

class Indicadores extends Component {
  state = {
      index: 0,
      routes: [
        { key: 'General', title: Languages.general },
        { key: 'Performance', title: Languages.performance },
        { key: 'Graphs', title: Languages.graphs },
      ],
    };

  _renderTabBar = (props) => <TabBar
    style={styles.tabBar}
    indicatorStyle={styles.inactiveColor}
    activeColor={Colors.blue}
    inactiveColor={Colors.lightBlue}
    tabStyle={styles.tabStyle}
    scrollEnabled={true}
    labelStyle={{ fontSize: 14 }}
    pressOpacity={1}
    {...props} />;

  render() {

    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({ General, Performance, Graphs })}
        onIndexChange={(index) => this.setState({ index })}
        renderTabBar={this._renderTabBar}
      />
    );
  }
}

const mapStateToProps = state => {
  return {

  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Indicadores)
