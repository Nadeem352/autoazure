import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import styles from './style';
import { Languages } from '@common';

class Performance extends Component {
  data=[
    {
      information: 'Preguntas / Visitors',
      percentage: '1.47%'
    },
    {
      information: 'Ventas / Visitors',
      percentage: '2.01%'
    },
    {
      information: 'Ventas / Preguntas',
      percentage: '136.54%'
    },
  ];

  render() {

    return (
      <ScrollView style={styles.container} contentContainerStyle={styles.performanceContainer}>
        <Text style={styles.headText}>{Languages.ultimos}</Text>
        { this.data.map(this._renderItem) }
      </ScrollView>
    );
  }

  _renderItem = (item) => (
    <View style={styles.itemView}>
      <Text style={styles.itemPercentage}>{item.percentage}</Text>
      <Text style={styles.itemInformation}>{item.information}</Text>
    </View>
  );

}

const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Performance)
