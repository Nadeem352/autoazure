import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import styles from './style';
import { Languages } from '@common';

class General extends Component {
  lines = [
    'Patience ensures victory.',
    'A graceful refusal is better than a lengthy promise.',
    'The best deed of a great man is to forgive and forget.',
    'Do not let your difficulties fill you with anxiety, after all it is only in the darkest nights that stars shine more brightly.',
  ];

  superScript = [
    {
      base: '$319',
      superScript: '00',
      detail: 'Ticket promedio'
    },
    {
      base: '$12.797',
      superScript: '62',
      detail: 'Dinero transaccionado'
    },
  ]

  render() {
    return (
      <ScrollView style={styles.container} contentContainerStyle={styles.generalContainer}>
        <View style={styles.barView}>
          <Text style={styles.barHead}>{Languages.compraymas.toUpperCase()}</Text>
          <Text style={styles.barHead}>Username</Text>
        </View>
        <View style={styles.colorBar}> 
          <View style={[styles.colorView,{backgroundColor:'white'}]}></View>
          <View style={[styles.colorView,{backgroundColor:'red'}]}></View>
          <View style={[styles.colorView,{backgroundColor:'orange'}]}></View>
          <View style={[styles.colorView,{backgroundColor:'yellow'}]}></View>
          <View style={[styles.colorView,{backgroundColor:'green'}]}></View>
          <View style={[styles.colorView,{backgroundColor:'pink'}]}></View>
        </View>
        <View style={styles.detailView}>
          <View style={styles.detailRow}>
            <Text style={styles.detailTitle}>{Languages.operacioners}</Text>
            <Text style={[styles.detailPercentage, styles.darkBlue]}>12.040</Text>
          </View>
          <View style={styles.detailRow}>
            <Text style={styles.detailTitle}>{Languages.reciamos}</Text>
            <Text style={styles.detailPercentage}>1.29%</Text>
          </View>
          <View style={styles.detailRow}>
            <Text style={styles.detailTitle}>{Languages.envios}</Text>
            <Text style={styles.detailPercentage}>9.26%</Text>
          </View>
        </View>
        <View style={styles.bulletsView}>
          {this.lines.map(this._renderBulletLine)}
        </View>
        <View>
          <View style={styles.footerHead}>
            <Text style={styles.barHead}>{Languages.actividad}</Text>
          </View>
          <View style={styles.footerDetail}>
            <View style={styles.detailRow}>
              <View style={styles.center}>
                <Text style={styles.detailPercentage}>40</Text>
                <Text style={styles.normalText}>Ventas</Text>
              </View>
              <View style={styles.center}>
                <Text style={styles.detailPercentage}>21</Text>
                <Text style={styles.normalText}>Preguntas</Text>
              </View>
            </View>
            {this.superScript.map(this._renderSuperScriptRow)}
          </View>
        </View>
      </ScrollView>
    );
  }

  _renderBulletLine = (line) => (
    <View style={styles.bulletRow}>
      <View style={styles.bullet} />
      <Text style={styles.normalText}>{line}</Text>
    </View>
  );
  _renderSuperScriptRow = (row) => (
    <View style={styles.center}>
      <View style={styles.row}>
        <Text style={styles.baseValue}>{row.base}</Text>
        <Text style={styles.superScript}>{row.superScript}</Text>
      </View>
      <Text style={styles.normalText}>{row.detail}</Text>
    </View>
  );
}

const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(General)
