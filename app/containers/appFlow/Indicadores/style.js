import { StyleSheet ,Dimensions} from 'react-native';
import { Colors, Constants } from '@common';
const { fontSize, Window,Dimension } = Constants;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ///////////// Performance ///////////////
  performanceContainer: {
    flex: 1,
    alignItems:'center',
    paddingVertical: 20
  },
  tabBar: {
    backgroundColor: Colors.offWhite
  },
  inactiveColor: {
    backgroundColor: Colors.blue
  },
  headText: {
    fontSize: fontSize.large,
    color: Colors.black,
    fontWeight:'bold'
  },
  itemInformation: {
    fontSize: fontSize.normal,
    color: Colors.black,
  },
  itemPercentage: {
    fontSize: fontSize.extraLarge,
    color: Colors.blue,
    fontWeight:'600'
  },
  itemView: {
    alignItems:'center',
    paddingVertical:10
  },
  /////////////// General ///////////////////////
  generalContainer: {
    paddingVertical:20,
    paddingHorizontal:20
  },
  barView: {
    marginVertical: 5
  },
  barHead: {
    fontSize: fontSize.medium,
    color: Colors.black,
    fontWeight:'bold',
    textAlign:'center',
  },
  detailView: {
    backgroundColor: Colors.offWhite,
    paddingHorizontal:10,
    paddingVertical:10,
    marginVertical: 5
  },
  detailTitle: {
    flex:.6,
    fontSize: fontSize.normal,
    color: Colors.black,
    fontWeight:'bold',
  },
  detailPercentage:{
    flex:.3,
    fontSize: fontSize.extraLarge,
    color: Colors.blue,
    fontWeight:'600'
  },
  detailRow:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    marginVertical:10
  },
  darkBlue:{
    color: Colors.darkBlue
  },
  bulletsView:{
    marginVertical:10,
  },
  bulletRow:{
    flexDirection: 'row',
    paddingVertical:5
  },
  bullet:{
    width:12,
    height:12,
    borderRadius:10,
    backgroundColor: Colors.careem,
    marginRight:10,
    marginTop:5
  },
  normalText:{
    flex:1,
    fontSize: fontSize.normal,
    color: Colors.black,
  },
  footerHead: {
    backgroundColor: Colors.offWhite,
    alignItems:'center',
    paddingVertical: 20
  },
  footerDetail: {
    marginHorizontal:50
  },
  center: {
    alignItems: 'center',
    paddingVertical:5
  },
  row: {
    flexDirection: 'row'
  },
  baseValue:{
    fontSize: fontSize.extraLarge,
    color: Colors.darkBlue,
    fontWeight:'600'
  },
  superScript: {
    fontSize: fontSize.normal,
    color: Colors.darkBlue,
  },
  colorBar:{
    height:22,
    width:200,
    backgroundColor:'transparent',
    alignSelf:'center',
    justifyContent:'center',
    flexDirection:'row',
    marginTop:10,
  },
  colorView:{
    height: 20,
    width: 20,
    backgroundColor: 'white',
    borderRadius: 100,
    borderColor:'black',
    borderWidth:1,
    marginHorizontal:3,
  },
  //////////////GRAPH////////////////
pickerView:{
  height: Dimension.ScreenHeight(0.07),
    width: Dimension.ScreenWidth(0.8),
  backgroundColor:'transparent',
  alignSelf:'center',
  justifyContent:'center',
  marginTop:20,
  borderWidth:0.5,
  borderColor:'grey'
},
  tabStyle: {
    width: Dimension.ScreenHeight(0.20)
  }

});

export default styles;
