import React, { Component } from 'react';
import { View, Text,} from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import styles from './style';
import RNPickerSelect from 'react-native-picker-select';
import { BarChart } from "react-native-chart-kit";
const data = {
  labels: ["January", "February", "March", "April", "May", "June"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43]
    }
  ]
};
class Graphs extends Component {
  render() {

    return (
      <View style={styles.container}>

       <View style={styles.pickerView}>
         <RNPickerSelect
             // placeholder={'Select an item'}
             textInputProps={{ paddingHorizontal: 10, color:'grey' }}
            onValueChange={(value) => console.log(value)}
            items={[
                { label: 'Football', value: 'football' },
                { label: 'Baseball', value: 'baseball' },
                { label: 'Hockey', value: 'hockey' },
            ]}
        />
        </View>
        <View style={styles.pickerView}>
         <RNPickerSelect
             // placeholder={'Select an item'}
             textInputProps={{ paddingHorizontal: 10, color:'grey' }}
             onValueChange={(value) => console.log(value)}
            items={[
                { label: 'Football', value: 'football' },
                { label: 'Baseball', value: 'baseball' },
                { label: 'Hockey', value: 'hockey' },
            ]}
        />
        </View>
        <View style={{ height: '50%',width: '100%', alignItems:'center', justifyContent:'center', marginTop: 30 }}>
          <BarChart
            // style={{
            //   height: '35%',
            //   width: '80%',
            //   alignSelf:'center'
            // }}
            withHorizontalLabels={true}
            data={data}
            width={350}
            height={270}
            withVerticalLabels={false}
            withHorizontalLabels={true}
            withInnerLines={true}
            yAxisSuffix={''}
            yAxisLabel={'$'}
            horizontalLabelRotation={0}
            verticalLabelRotation={0}
            showBarTops={true}
            chartConfig={{
              backgroundColor: "#f1f1f1",
              backgroundGradientFrom: "#f1f1f1",
              backgroundGradientTo: "#fff",
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => '#38B1E7',
              labelColor: (opacity = 1) => '#38B1E7',
              style: {
                borderRadius: 16
              },
              propsForDots: {
                r: "6",
                strokeWidth: "2",
                stroke: "#ffa726"
              }
            }}
            verticalLabelRotation={30} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  let data = state.loginData;
  return {
    loading: data.loading,
    status: data.status,
    error: data.error,
    loginData: data.data
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Graphs)
