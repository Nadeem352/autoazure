import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import AppNavigation from './navigation/appNavigation';
import { Provider } from "react-redux";
import store, { persistor } from './redux/store/configureStore';
import { PersistGate } from 'redux-persist/lib/integration/react';
import Loading  from './components/shared/loading';
import SplashScreen from 'react-native-splash-screen'

export default class App extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount()
  {
    SplashScreen.hide();
  }
  render() {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#0091ea' }}>
          <Provider store={store}>
            <PersistGate loading={<Loading styles = {{flex:1}} />} persistor={persistor}>
              <AppNavigation />
            </PersistGate>
          </Provider>
        </SafeAreaView>
    );
  }
}
