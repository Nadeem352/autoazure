class Api {
    static headers() {
      return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'dataType': 'json',
      }
    }
  
    static get(route, params) {
      return this.xhr(route, null, 'GET');
    }
  
    static put(route, params) {
      return this.xhr(route, params, 'PUT')
    }
  
    static post(route, params) {      
      return this.xhr(route, params, 'POST')
    }
  
    static delete(route, params) {
      return this.xhr(route, params, 'DELETE')
    }
  
    static xhr(route, params, verb) {      
      
      const host = 'http://dev-erp-autoazur.azurewebsites.net/Login.aspx/LoginSystemUser'
      // const host = 'http://192.168.0.129:4000'
      const url = `${host}`
      let obj = {
        "login":
        {
            "CompanyCode": params.company,
            "UserName": params.name,
            "Password": params.password
        }
      }
      let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(obj) } : null );
      options.headers = Api.headers();
      return fetch(url, options).then( resp => {
        console.log('response ', resp)
        let json = resp.json();   
        if (resp.ok) {          
            return json
        }
        return json.then( err => {throw err});
      }).then( res => { 
        return res;
      });
    }
  }
  export default Api
  