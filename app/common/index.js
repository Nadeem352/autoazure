/** @format */

import _Color from "./Color";
import _Constants from "./Constants";
import _Images from "./Images";
import _Languages from "./Languages";


export const Colors = _Color;
export const Constants = _Constants;
export const Images = _Images;
export const Languages = _Languages;

