/**
 * Created by InspireUI on 20/12/2016.
 *
 * @format
 */

import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const Constants = {
  fontSize: {
    tiny:12,
    small:14,
    normal:16,
    medium:18,
    large:20,
    extraLarge:24
  },
  Dimension: {
    ScreenWidth(percent = 1) {
      return Dimensions.get("window").width * percent;
    },
    ScreenHeight(percent = 1) {
      return Dimensions.get("window").height * percent;
    },
  },
  Window: {
    width,
    height,
    headerHeight: (65 * height) / 100,
    headerBannerAndroid: (55 * height) / 100,
    profileHeight: (45 * height) / 100,
    behavior: Platform.OS === 'ios' ? 'padding':'height'
  }
};

export default Constants;
