/**
 * Created by InspireUI on 17/02/2017.
 *
 * @format
 */

const images = {
  // leave off @2x/@3x
  bgSplash: require('../assets/images/splash-bg.png'),
  bgWelcome: require('../assets/images/welcome-bg.png'),
  bgLogin: require('../assets/images/login-bg.png'),
  logo: require('../assets/images/shape4Copy.png'),
  nextHireText: require('../assets/images/nextHire.png'),
  
  icons: {
    
  },
  Banner: {
   
  }
};

export default images;
