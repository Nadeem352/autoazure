/** @format */



export default {
  welcome: 'Welcome',
  agreement:'By tapping Login, you agree to our ',
  terms: 'Terms ',
  and:'and ',
  privacyPolicy:'Privacy Policy',
  loginNow:'Login Now',
  createAccount: 'Create Account',
  troubleLogin: 'Trouble Logging In?',
  postMessage:"We don't post anything to Facebook.",
  company:"Company",
  done:'done',
  login:'Login',
  log_in:"Log In",
  username:'Username',
  password:"Password",
  register:"Register Now",
  ultimos:'Ultimos 7 dias',
  general:'General',
  performance: 'Performance',
  graphs: 'Graphs',
  activas: 'Activas',
  historial: 'Historial',
  actives: 'ABIERTAS', //HISTORICO
  closed: 'CERRADAS', //HISTORICO
  unread: 'NO LEIDOS',
  compraymas:'Compraymas2018',
  operacioners:'Operaciones',
  reciamos:'Reciamos',
  envios:'Envios con despacho demorado',
  actividad:'Actividad de hoy'
}
